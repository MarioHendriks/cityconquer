import { Controller, Get } from '@nestjs/common';
import { WorldServiceService } from './world-service.service';

@Controller()
export class WorldServiceController {
  constructor(private readonly worldServiceService: WorldServiceService) {}

  @Get()
  getHello(): string {
    return this.worldServiceService.getHello();
  }
}
