import { Test, TestingModule } from '@nestjs/testing';
import { WorldServiceController } from './world-service.controller';
import { WorldServiceService } from './world-service.service';

describe('WorldServiceController', () => {
  let worldServiceController: WorldServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [WorldServiceController],
      providers: [WorldServiceService],
    }).compile();

    worldServiceController = app.get<WorldServiceController>(WorldServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(worldServiceController.getHello()).toBe('Hello World!');
    });
  });
});
