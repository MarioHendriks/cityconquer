import { Module } from '@nestjs/common';
import { WorldServiceController } from './world-service.controller';
import { WorldServiceService } from './world-service.service';

@Module({
  imports: [],
  controllers: [WorldServiceController],
  providers: [WorldServiceService],
})
export class WorldServiceModule {}
