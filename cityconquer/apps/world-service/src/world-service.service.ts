import { Injectable } from '@nestjs/common';

@Injectable()
export class WorldServiceService {
  getHello(): string {
    return 'Hello World!';
  }
}
