import { NestFactory } from '@nestjs/core';
import { WorldServiceModule } from './world-service.module';

async function bootstrap() {
  const app = await NestFactory.create(WorldServiceModule);
  await app.listen(3000);
}
bootstrap();
