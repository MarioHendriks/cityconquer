import { Controller, Get } from '@nestjs/common';
import { MapServiceService } from './map-service.service';

@Controller()
export class MapServiceController {
  constructor(private readonly mapServiceService: MapServiceService) {}

  @Get()
  getHello(): string {
    return this.mapServiceService.getHello();
  }
}
