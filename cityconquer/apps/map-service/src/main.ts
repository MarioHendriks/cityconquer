import { NestFactory } from '@nestjs/core';
import { MapServiceModule } from './map-service.module';

async function bootstrap() {
  const app = await NestFactory.create(MapServiceModule);
  await app.listen(3000);
}
bootstrap();
