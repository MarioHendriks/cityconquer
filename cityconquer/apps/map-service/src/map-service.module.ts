import { Module } from '@nestjs/common';
import { MapServiceController } from './map-service.controller';
import { MapServiceService } from './map-service.service';

@Module({
  imports: [],
  controllers: [MapServiceController],
  providers: [MapServiceService],
})
export class MapServiceModule {}
