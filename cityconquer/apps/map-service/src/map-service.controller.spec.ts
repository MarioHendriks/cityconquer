import { Test, TestingModule } from '@nestjs/testing';
import { MapServiceController } from './map-service.controller';
import { MapServiceService } from './map-service.service';

describe('MapServiceController', () => {
  let mapServiceController: MapServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MapServiceController],
      providers: [MapServiceService],
    }).compile();

    mapServiceController = app.get<MapServiceController>(MapServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(mapServiceController.getHello()).toBe('Hello World!');
    });
  });
});
