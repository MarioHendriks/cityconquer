import { Module } from '@nestjs/common';
import { BuildingServiceController } from './building-service.controller';
import { BuildingServiceService } from './building-service.service';

@Module({
  imports: [],
  controllers: [BuildingServiceController],
  providers: [BuildingServiceService],
})
export class BuildingServiceModule {}
