import { NestFactory } from '@nestjs/core';
import { BuildingServiceModule } from './building-service.module';

async function bootstrap() {
  const app = await NestFactory.create(BuildingServiceModule);
  await app.listen(3000);
}
bootstrap();
