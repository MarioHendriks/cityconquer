import { Controller, Get } from '@nestjs/common';
import { BuildingServiceService } from './building-service.service';

@Controller()
export class BuildingServiceController {
  constructor(private readonly buildingServiceService: BuildingServiceService) {}

  @Get()
  getHello(): string {
    return this.buildingServiceService.getHello();
  }
}
