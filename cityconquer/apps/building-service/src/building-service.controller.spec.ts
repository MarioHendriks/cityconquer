import { Test, TestingModule } from '@nestjs/testing';
import { BuildingServiceController } from './building-service.controller';
import { BuildingServiceService } from './building-service.service';

describe('BuildingServiceController', () => {
  let buildingServiceController: BuildingServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [BuildingServiceController],
      providers: [BuildingServiceService],
    }).compile();

    buildingServiceController = app.get<BuildingServiceController>(BuildingServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(buildingServiceController.getHello()).toBe('Hello World!');
    });
  });
});
