import { Module } from '@nestjs/common';
import { Profile=serviceController } from './profile=service.controller';
import { Profile=serviceService } from './profile=service.service';

@Module({
  imports: [],
  controllers: [Profile=serviceController],
  providers: [Profile=serviceService],
})
export class Profile=serviceModule {}
