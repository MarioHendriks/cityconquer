import { Test, TestingModule } from '@nestjs/testing';
import { Profile=serviceController } from './profile=service.controller';
import { Profile=serviceService } from './profile=service.service';

describe('Profile=serviceController', () => {
  let profile=serviceController: Profile=serviceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [Profile=serviceController],
      providers: [Profile=serviceService],
    }).compile();

    profile=serviceController = app.get<Profile=serviceController>(Profile=serviceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(profile=serviceController.getHello()).toBe('Hello World!');
    });
  });
});
