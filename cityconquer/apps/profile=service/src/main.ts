import { NestFactory } from '@nestjs/core';
import { Profile=serviceModule } from './profile=service.module';

async function bootstrap() {
  const app = await NestFactory.create(Profile=serviceModule);
  await app.listen(3000);
}
bootstrap();
