import { Test, TestingModule } from '@nestjs/testing';
import { ArmyServiceController } from './army-service.controller';
import { ArmyServiceService } from './army-service.service';

describe('ArmyServiceController', () => {
  let armyServiceController: ArmyServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ArmyServiceController],
      providers: [ArmyServiceService],
    }).compile();

    armyServiceController = app.get<ArmyServiceController>(ArmyServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(armyServiceController.getHello()).toBe('Hello World!');
    });
  });
});
