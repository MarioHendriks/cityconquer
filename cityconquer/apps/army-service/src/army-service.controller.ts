import { Controller, Get } from '@nestjs/common';
import { ArmyServiceService } from './army-service.service';

@Controller()
export class ArmyServiceController {
  constructor(private readonly armyServiceService: ArmyServiceService) {}

  @Get()
  getHello(): string {
    return this.armyServiceService.getHello();
  }
}
