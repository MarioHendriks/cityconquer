import { Module } from '@nestjs/common';
import { ArmyServiceController } from './army-service.controller';
import { ArmyServiceService } from './army-service.service';

@Module({
  imports: [],
  controllers: [ArmyServiceController],
  providers: [ArmyServiceService],
})
export class ArmyServiceModule {}
