import { NestFactory } from '@nestjs/core';
import { ArmyServiceModule } from './army-service.module';

async function bootstrap() {
  const app = await NestFactory.create(ArmyServiceModule);
  await app.listen(3000);
}
bootstrap();
