import { Module } from '@nestjs/common';
import { DiplomaticServiceController } from './diplomatic-service.controller';
import { DiplomaticServiceService } from './diplomatic-service.service';

@Module({
  imports: [],
  controllers: [DiplomaticServiceController],
  providers: [DiplomaticServiceService],
})
export class DiplomaticServiceModule {}
