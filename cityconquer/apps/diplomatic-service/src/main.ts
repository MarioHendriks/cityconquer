import { NestFactory } from '@nestjs/core';
import { DiplomaticServiceModule } from './diplomatic-service.module';

async function bootstrap() {
  const app = await NestFactory.create(DiplomaticServiceModule);
  await app.listen(3000);
}
bootstrap();
