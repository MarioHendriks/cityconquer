import { Controller, Get } from '@nestjs/common';
import { DiplomaticServiceService } from './diplomatic-service.service';

@Controller()
export class DiplomaticServiceController {
  constructor(private readonly diplomaticServiceService: DiplomaticServiceService) {}

  @Get()
  getHello(): string {
    return this.diplomaticServiceService.getHello();
  }
}
