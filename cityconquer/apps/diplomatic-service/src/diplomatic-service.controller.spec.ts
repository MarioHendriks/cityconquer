import { Test, TestingModule } from '@nestjs/testing';
import { DiplomaticServiceController } from './diplomatic-service.controller';
import { DiplomaticServiceService } from './diplomatic-service.service';

describe('DiplomaticServiceController', () => {
  let diplomaticServiceController: DiplomaticServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DiplomaticServiceController],
      providers: [DiplomaticServiceService],
    }).compile();

    diplomaticServiceController = app.get<DiplomaticServiceController>(DiplomaticServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(diplomaticServiceController.getHello()).toBe('Hello World!');
    });
  });
});
