import { Entity, Column, PrimaryGeneratedColumn, Unique, CreateDateColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
 
  @Column({nullable: false, unique: true})
  username!: string;

  @CreateDateColumn()
    createdDate: Date;
}
