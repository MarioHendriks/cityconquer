import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './database/user.entity';
import { UserServiceController } from './user-service.controller';
import { UserServiceService } from './user-service.service';
import * as Joi from 'joi';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: new ConfigService().get<string>('TYPEORM_HOST'),
    port: +new ConfigService().get<string>('TYPEORM_PORT'),
    username: "postgres", //TODO clean this code
    password: "postgres",
    database: new ConfigService().get<string>('TYPEORM_DATABASE'),
    entities: [User],
    synchronize: true,
  }),
  TypeOrmModule.forFeature([User]),
  ],
  controllers: [UserServiceController],
  providers: [UserServiceService],
})
export class UserServiceModule { }
