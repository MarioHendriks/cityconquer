import BadRequestException from '@lib/exceptions/BadRequestException';
import InternalServerException from '@lib/exceptions/InternalServerException';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import CreateDTO from './models/create.dto';
import UserViewmodel from './models/user.viewmodel';
import { UserServiceService } from './user-service.service';

const typeOrmErr = {
  DUPE_ENTRY: 23505,
};

@Controller('user')
export class UserServiceController {
  constructor(private readonly userServiceService: UserServiceService) {}

  @Get('/:id')
  public async getUser(@Param() id): Promise<UserViewmodel> {
    return this.userServiceService.getUserById(id).then(res => {
      if(!res) throw new BadRequestException(`Can't find id:${id.id}`)
      return res
    })
  }

  @Post()
  public async createUser(@Body() createDTO: CreateDTO): Promise<UserViewmodel>{
    return this.userServiceService.createUser(createDTO).then(res => {
      return res
    }).catch((err: { message: string; code: string; detail: string }) => {
      let message = err.message;
      if (+err.code === typeOrmErr.DUPE_ENTRY) {
        let startIndex = err.detail.indexOf('(') + 1;
        let length = err.detail.indexOf(')') - startIndex;
        let errorKey = err.detail.substr(startIndex, length);

        message = `this ${errorKey} is already in use.`;
      }
      throw new BadRequestException(message);
    })

  }
}
