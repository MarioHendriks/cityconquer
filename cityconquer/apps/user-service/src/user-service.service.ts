import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './database/user.entity';
import createDto from './models/create.dto';
import UserViewmodel from './models/user.viewmodel';

@Injectable()
export class UserServiceService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ){}


  getUserById(id: any): Promise<UserViewmodel> {
    return this.userRepository.findOne(id).then(res => {
      return res as UserViewmodel
    })
  }

  async createUser(createDTO: createDto): Promise<UserViewmodel> {
    throw await this.userRepository.save(createDTO as User)
  }
}
