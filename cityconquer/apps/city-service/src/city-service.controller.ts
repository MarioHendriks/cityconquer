import { Controller, Get } from '@nestjs/common';
import { CityServiceService } from './city-service.service';

@Controller()
export class CityServiceController {
  constructor(private readonly cityServiceService: CityServiceService) {}

  @Get()
  getHello(): string {
    return this.cityServiceService.getHello();
  }
}
