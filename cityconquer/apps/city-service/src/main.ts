import { NestFactory } from '@nestjs/core';
import { CityServiceModule } from './city-service.module';

async function bootstrap() {
  const app = await NestFactory.create(CityServiceModule);
  await app.listen(3000);
}
bootstrap();
