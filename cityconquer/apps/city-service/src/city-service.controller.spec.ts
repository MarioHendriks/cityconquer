import { Test, TestingModule } from '@nestjs/testing';
import { CityServiceController } from './city-service.controller';
import { CityServiceService } from './city-service.service';

describe('CityServiceController', () => {
  let cityServiceController: CityServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [CityServiceController],
      providers: [CityServiceService],
    }).compile();

    cityServiceController = app.get<CityServiceController>(CityServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(cityServiceController.getHello()).toBe('Hello World!');
    });
  });
});
