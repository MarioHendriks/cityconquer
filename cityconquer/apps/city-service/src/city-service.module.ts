import { Module } from '@nestjs/common';
import { CityServiceController } from './city-service.controller';
import { CityServiceService } from './city-service.service';

@Module({
  imports: [],
  controllers: [CityServiceController],
  providers: [CityServiceService],
})
export class CityServiceModule {}
