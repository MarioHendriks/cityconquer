import { Test, TestingModule } from '@nestjs/testing';
import { CombatServiceController } from './combat-service.controller';
import { CombatServiceService } from './combat-service.service';

describe('CombatServiceController', () => {
  let combatServiceController: CombatServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [CombatServiceController],
      providers: [CombatServiceService],
    }).compile();

    combatServiceController = app.get<CombatServiceController>(CombatServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(combatServiceController.getHello()).toBe('Hello World!');
    });
  });
});
