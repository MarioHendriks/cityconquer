import { Controller, Get } from '@nestjs/common';
import { CombatServiceService } from './combat-service.service';

@Controller()
export class CombatServiceController {
  constructor(private readonly combatServiceService: CombatServiceService) {}

  @Get()
  getHello(): string {
    return this.combatServiceService.getHello();
  }
}
