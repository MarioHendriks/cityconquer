import { Module } from '@nestjs/common';
import { CombatServiceController } from './combat-service.controller';
import { CombatServiceService } from './combat-service.service';

@Module({
  imports: [],
  controllers: [CombatServiceController],
  providers: [CombatServiceService],
})
export class CombatServiceModule {}
