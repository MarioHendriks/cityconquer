import { NestFactory } from '@nestjs/core';
import { CombatServiceModule } from './combat-service.module';

async function bootstrap() {
  const app = await NestFactory.create(CombatServiceModule);
  await app.listen(3000);
}
bootstrap();
