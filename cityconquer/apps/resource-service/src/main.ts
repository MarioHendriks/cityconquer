import { NestFactory } from '@nestjs/core';
import { ResourceServiceModule } from './resource-service.module';

async function bootstrap() {
  const app = await NestFactory.create(ResourceServiceModule);
  await app.listen(3000);
}
bootstrap();
