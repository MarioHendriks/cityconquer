import { Test, TestingModule } from '@nestjs/testing';
import { SpawnServiceController } from './spawn-service.controller';
import { SpawnServiceService } from './spawn-service.service';

describe('SpawnServiceController', () => {
  let spawnServiceController: SpawnServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [SpawnServiceController],
      providers: [SpawnServiceService],
    }).compile();

    spawnServiceController = app.get<SpawnServiceController>(SpawnServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(spawnServiceController.getHello()).toBe('Hello World!');
    });
  });
});
