import { NestFactory } from '@nestjs/core';
import { SpawnServiceModule } from './spawn-service.module';

async function bootstrap() {
  const app = await NestFactory.create(SpawnServiceModule);
  await app.listen(3000);
}
bootstrap();
