import { Controller, Get } from '@nestjs/common';
import { SpawnServiceService } from './spawn-service.service';

@Controller()
export class SpawnServiceController {
  constructor(private readonly spawnServiceService: SpawnServiceService) {}

  @Get()
  getHello(): string {
    return this.spawnServiceService.getHello();
  }
}
