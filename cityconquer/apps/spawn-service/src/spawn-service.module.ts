import { Module } from '@nestjs/common';
import { SpawnServiceController } from './spawn-service.controller';
import { SpawnServiceService } from './spawn-service.service';

@Module({
  imports: [],
  controllers: [SpawnServiceController],
  providers: [SpawnServiceService],
})
export class SpawnServiceModule {}
